const mongoose = require('mongoose');
const validator = require('validator');

const gameSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: mongoose.Schema.Types.String,
        required: true,
        maxlength: 80
    },
    url: {
        type: mongoose.Schema.Types.String,
        required: true,
        maxlength: 160,
        validate: {
            validator: function (value) {
                return validator.isURL(value);
            },
            message: 'Invalid URL'
        }
    },
    like: {
        type: mongoose.Schema.Types.Number,
        default: 0
    } 
});

const Game = mongoose.model('Game', gameSchema);

module.exports = Game;
