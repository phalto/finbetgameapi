const gamesService = require('../services/games');

const getAllGames = async (req, res, next) => {
  try {
    const allGames = await gamesService.getAllGames();
    res.status(200).json(allGames);
  } catch (error) {
    next(error);
  }
};

const getGameByName = async (req, res, next) => {
  const name = req.params.name;

  try {
    if (name == undefined) {
      const error = new Error('Nedostaje naziv igre!');
      error.status = 400;
      throw error;
    }

    const game = await gamesService.getGameByName(name);
    if (game == null) {
      res.status(404).json();
    } else {
      res.status(200).json(game);
    }
  } catch (error) {
    next(error);
  }
};

const addNewGame = async (req, res, next) => {
  const { name, url, like } = req.body;

  try {
    if (!name || !url) {
      const error = new Error('Proverite prosledjene podatke!');
      error.status = 400;
      throw error;
    }

    const game = await gamesService.getGameByName(name);
    if (game) {
      const error = new Error('Ovo ime igre vec postoji!');
      error.status = 403;
      throw error;
    }

    const newGame = await gamesService.addNewGame(
      name,
      url,
      like
    );
    res.status(201).json(newGame);
  } catch (error) {
    next(error);
  }
};

const likeGame = async (req, res, next) => {
  const { name } = req.body;

  try {
    if (!name) {
      const error = new Error('Fali ime igre');
      error.status = 400;
      throw error;
    }

    const game = await gamesService.getGameByName(name);

    if (!game) {
      const error = new Error('Ne postoji igra sa ovim imenom!');
      error.status = 404;
      throw error;
    }

    const updatedGame = await gamesService.likeGame(name);

    if (updatedGame) {
      res.status(200).json(updatedGame);
    } else {
      const error = new Error('Nije uspelo lajkovanje igre');
      error.status = 403;
      throw error;
    }
  } catch (error) {
    next(error);
  }
};

const deleteGame = async (req, res, next) => {
  const name = req.params.name;

  try {
    if (!name) {
      const error = new Error('Nedostaje ime igre!');
      error.status = 400;
      throw error;
    }

    const game = await gamesService.getGameByName(name);
    if (!game) {
      const error = new Error('Proverite ime igre!');
      error.status = 404;
      throw error;
    }

    await gamesService.deleteGame(name);
    res.status(200).json();
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getGameByName,
  getAllGames,
  addNewGame,
  likeGame,
  deleteGame,
};
