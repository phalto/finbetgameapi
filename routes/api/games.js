const express = require('express');
const gamesController = require('../../contollers/games');

const router = express.Router();

router.get('/', gamesController.getAllGames);
router.get('/:name', gamesController.getGameByName);

router.post('/', gamesController.addNewGame);
router.put('/', gamesController.likeGame);
router.delete('/:name', gamesController.deleteGame);

module.exports = router;
