const mongoose = require('mongoose');
const Game = require('../models/games');

const getAllGames = async () => {
  const games = await Game.find({}).exec();
  return games;
};


const getGameByName = async (name) => {
  const game = await Game.findOne({ name: name }).exec();
  return game;
};


const addNewGame = async (name, url, like) => {
  const newGame = new Game({
    _id: new mongoose.Types.ObjectId(),
    name,
    url,
    like,
  });
  await newGame.save();
  return newGame;
};

const likeGame = async (name) => {
  const updatedGame = await Game.findOneAndUpdate(
    { name: name },
    { $inc: { like: 1} },
    { new: true }
  );

  return updatedGame;
};

const deleteGame = async (name) => {
  await Game.findOneAndDelete({ name: name }).exec();
};

module.exports = {
  getAllGames,
  getGameByName,
  addNewGame,
  likeGame,
  deleteGame,
};
