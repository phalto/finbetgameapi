## Prerequisites

- [Node.js](https://nodejs.org/en)
- [npm](https://www.npmjs.com/)

## Database

MongoDB running at localhost:27017/.

## Build

Clone and install required dependencies:

```sh
git clone https://gitlab.com/phalto/finbetgameapi
cd finbetgameapi
npm init
npm install
nodemon server.js
```

## Database schemas

**Game:**

| Field | Type | Description |
| ------ | ------ | -------- |
| id | ObjectId | Unique value| 
| name | String | Game name | 
| url | String | Game url |
| like |  Number  | Number of likes |

## Games API:

| HTTP method | Path | Description |
| ------ | ------ | -------- |
| GET | /api/games/ | Show all games| 
| GET | /api/games/{name} | Show one game (name specified) | 
| POST | /api/games/ | Add new game |
| PUT |  /api/games/  | Like a game |
| DELETE |  /api/games/{name}  | Delete one game (name specified) |
